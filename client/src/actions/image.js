import axios from "axios";

export const search = async (query, callback) => {
    try {
        const res = await axios.get("/api/image/search", {
            params: { query }
        });
        callback(res.data.images);
    } catch (err) {
        throw new Error(err);
    }
};

export const random = async callback => {
    try {
        const res = await axios.get("/api/image/random");
        callback(res.data.images);
    } catch (err) {
        throw new Error(err);
    }
};

export const download = pictureId => {
    try {
        fetch(
            `https://api.unsplash.com/photos/${pictureId}/download?client_id=0f24e7578c59016da724854b85fefc2b3cfb0b9ccec28699763ccde059e293af`
        )
            .then(res => res.json())
            .then(result => console.log(result));
    } catch (err) {
        throw new Error(err);
    }
};
