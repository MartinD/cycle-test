import axios from "axios";

export const list = async callback => {
    try {
        const res = await axios.get("/api/user");
        callback(res.data.users);
    } catch (err) {
        throw new Error(err);
    }
};

export const create = async ({ email, firstName, role }, callback) => {
    try {
        const res = await axios.post("/api/user", { email, firstName, role });
        callback(res.data.users);
    } catch (err) {
        throw new Error(err);
    }
};

export const update = async (userId, { email, firstName, role }, callback) => {
    try {
        const res = await axios.put(`/api/user/${userId}`, {
            email,
            firstName,
            role
        });
        callback(res.data.users);
    } catch (err) {
        throw new Error(err);
    }
};

export const destroy = async (userId, callback) => {
    try {
        const res = await axios.delete(`/api/user/${userId}`);
        callback(res.data.users);
    } catch (err) {
        throw new Error(err);
    }
};
