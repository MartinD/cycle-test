import axios from "axios";

export const login = async ({ email, password }) => {
    try {
        const res = await axios.post("/api/auth/login", { email, password });
        return res;
    } catch (err) {
        throw new Error(err);
    }
};

export const logout = async () => {
    try {
        const res = await axios.post("/api/auth/logout");
        return res;
    } catch (err) {
        throw new Error(err);
    }
};

export const verifyToken = async () => {
    try {
        const res = await axios.get("/api/auth");
        return res;
    } catch (err) {
        throw new Error(err);
    }
};

export const update = async (
    { email, firstName, avatar, password, confirmPassword },
    callback
) => {
    try {
        const res = await axios.put("/api/auth", {
            email,
            firstName,
            avatar,
            password,
            confirmPassword
        });
        callback(JSON.stringify(res.data.user));
    } catch (err) {
        throw new Error(err);
    }
};
