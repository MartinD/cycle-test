import React from "react";
import ReactDOM from "react-dom";
import { Style } from "./index.style";
import "typeface-roboto";

import Routes from "./pages/Routes";

ReactDOM.render(
    <div>
        <Style />
        <Routes />
    </div>,
    document.getElementById("mount")
);
