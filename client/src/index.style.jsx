import { createGlobalStyle } from "styled-components";

export const Style = createGlobalStyle`

    @import url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap');

    @font-face {
        font-family: 'Roboto';
        font-style: normal;
        font-weight: 400;
        src: url('https://fonts.googleapis.com/css?family=Roboto:300,400,500,700&display=swap'); 
    }

    body {
        font-family: "Roboto";
    }
`;
