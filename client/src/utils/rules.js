const rules = {
    Reader: {
        permissions: ["listUsers", "updateAuth"]
    },

    Contributor: {
        inherit: "Reader",
        permissions: ["updateUser"]
    },

    Admin: {
        inherit: "Contributor",
        permissions: ["createUser", "destroyUser"]
    }
};

export const check = (role, action) => {
    const rights = rules[role];
    if (!rights) {
        return false;
    }
    if (rights.permissions && rights.permissions.includes(action)) {
        return true;
    } else if (rights.inherit) {
        return check(rights.inherit, action);
    }
    return false;
};
