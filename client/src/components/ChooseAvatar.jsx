import React, { useState, useEffect } from "react";
import ClearIcon from "@material-ui/icons/Clear";
import IconButton from "@material-ui/core/IconButton";
import TextField from "@material-ui/core/TextField";
import { download, random, search } from "../actions/image";

export const ChooseAvatar = ({ user, setUser, onCancel }) => {
    const [unsplashPictures, setUnsplashPictures] = useState([]);
    useEffect(() => {
        random(setUnsplashPictures);
    }, []);
    const onSearchPictures = value => {
        if (value !== "") {
            search(value, setUnsplashPictures);
        } else {
            random(setUnsplashPictures);
        }
    };
    return (
        <div>
            <div
                style={{
                    display: "flex",
                    alignItems: "center",
                    marginBottom: "1em"
                }}
            >
                <TextField
                    label={"Search for a picture"}
                    variant="outlined"
                    onChange={e => onSearchPictures(e.target.value)}
                />
                <IconButton
                    aria-haspopup="true"
                    color="inherit"
                    onClick={onCancel}
                >
                    <ClearIcon />
                </IconButton>
            </div>
            <div
                style={{
                    height: "200px",
                    overflow: "auto",
                    marginBottom: "30px"
                }}
            >
                {unsplashPictures.map(value => {
                    return (
                        <div
                            key={value.picture}
                            type={value.picture}
                            current={value.picture}
                            onClick={e => {
                                let newUser = { ...user };
                                newUser.avatar = e.currentTarget.getAttribute(
                                    "type"
                                );
                                download(value.pictureId);
                                setUser(newUser);
                                onCancel();
                            }}
                            style={{
                                position: "relative",
                                display: "inline-flex",
                                margin: "3px",
                                padding: "3px",
                                cursor: "pointer",
                                borderRadius: "5px",
                                alignItems: "center",
                                justifyContent: "center",
                                border: `2px solid ${
                                    value.picture === user.avatar
                                        ? "black"
                                        : "white"
                                }`
                            }}
                        >
                            <img
                                style={{
                                    height: "80px",
                                    width: "150px",
                                    borderRadius: "5px",
                                    objectFit: "cover"
                                }}
                                src={value.picture}
                            />
                            <a
                                href={`https://unsplash.com/@${value.username}?utm_source=PriceIt&utm_medium=referral`}
                                target="_blank"
                                style={{
                                    position: "absolute",
                                    bottom: "5px",
                                    left: "10px",
                                    color: "white",
                                    fontSize: "10px"
                                }}
                            >
                                {value.name}
                            </a>
                        </div>
                    );
                })}
            </div>
        </div>
    );
};
