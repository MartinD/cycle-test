import React, { useContext } from "react";
import { useHistory } from "react-router";
import AppBar from "@material-ui/core/AppBar";
import Typography from "@material-ui/core/Typography";
import MenuItem from "@material-ui/core/MenuItem";
import Menu from "@material-ui/core/Menu";
import IconButton from "@material-ui/core/IconButton";
import { AuthContext } from "../context/AuthContext";
import { Avatar, Content, ToolbarStyled } from "./Menu.style";

const HeadMenu = ({ children }) => {
    const [anchorEl, setAnchorEl] = React.useState(null);
    const history = useHistory();
    let { auth, setAuth } = useContext(AuthContext);
    auth = JSON.parse(auth);

    const isMenuOpen = Boolean(anchorEl);

    const handleProfileMenuOpen = event => {
        setAnchorEl(event.currentTarget);
    };
    const handleMenuClose = () => {
        setAnchorEl(null);
    };

    return (
        <div>
            <AppBar position="static">
                <ToolbarStyled>
                    <Typography variant="h6">
                        Hello {auth ? auth.firstName : ""}!
                    </Typography>
                    <IconButton
                        aria-haspopup="true"
                        color="inherit"
                        onClick={handleProfileMenuOpen}
                    >
                        <Avatar
                            src={
                                auth && auth.avatar
                                    ? auth.avatar
                                    : "https://cycle.app/public/img/favicon@2x.png"
                            }
                        />
                    </IconButton>
                </ToolbarStyled>
            </AppBar>
            <Menu
                anchorEl={anchorEl}
                anchorOrigin={{ vertical: "top", horizontal: "right" }}
                keepMounted
                transformOrigin={{ vertical: "top", horizontal: "right" }}
                open={isMenuOpen}
                onClose={handleMenuClose}
            >
                <MenuItem onClick={() => history.push("/profile")}>
                    Profile
                </MenuItem>
                <MenuItem onClick={() => setAuth(null)}>Logout</MenuItem>
            </Menu>
            <Content>{children}</Content>
        </div>
    );
};

export default HeadMenu;
