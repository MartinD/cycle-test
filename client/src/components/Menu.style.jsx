import styled from "styled-components";
import Toolbar from "@material-ui/core/Toolbar";

export const Avatar = styled.img`
    width: 40px;
    border-radius: 50%;
    height: 40px;
`;

export const ToolbarStyled = styled(Toolbar)`
    display: flex;
    justify-content: space-between;
`;

export const Content = styled.div`
    margin: 2em;
`;
