import styled from "styled-components";
import TextField from "@material-ui/core/TextField";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";
import Button from "@material-ui/core/Button";

export const CardStyled = styled(Card)`
    margin-top: 1em;
`;

export const CardContentStyled = styled(CardContent)`
    display: flex;
    flex-direction: column;
    justify-content: center;
`;

export const AvatarContainer = styled.div`
    display: flex;
    justify-content: center;
`

export const Avatar = styled.img`
    width: 200px;
    border-radius: 50%;
    height: 200px;
`;

export const WrapperTextField = styled.div`
    margin-bottom: 1em;
`;

export const TextFieldStyled = styled(TextField)`
    width: 100%;
`;

export const Role = styled.div`
    display: flex;
    justify-content: center;
    padding: 1em;
`;

export const ButtonGroup = styled.div`
    display: flex;
    justify-content: flex-end;
`;

export const WrapperButton = styled(Button)`
    margin-right: 1em;
`;
