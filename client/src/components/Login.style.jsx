import styled, { keyframes } from "styled-components";
import TextField from "@material-ui/core/TextField";
import CardContent from "@material-ui/core/CardContent";
import Card from "@material-ui/core/Card";

export const CardContentStyled = styled(CardContent)`
    display: flex;
    flex-direction: column;
`;

const manifestoRotate = keyframes`
    100% {
        transform: rotate(360deg);
    }
`;
export const Circle = styled.div`
    background-size: cover;
    transition: all 250ms ease-out;
    background-image: url("https://cycle.app/public/img/circle@2x.png");
    animation: ${manifestoRotate} 12s infinite linear;
    -moz-animation: ${manifestoRotate} 12s infinite linear;
    -o-animation: ${manifestoRotate} 12s infinite linear;
    -webkit-animation: ${manifestoRotate} 12s infinite linear;
    height: 700px;
    width: 700px;
    overflow: hidden;
`;

export const WrapperTextField = styled.div`
    margin-bottom: 1em;
`;

export const TextFieldStyled = styled(TextField)`
    width: 100%;
`;

export const Container = styled.div`
    background-color: black;
    width: 100%;
    height: 100%;
    overflow: hidden;
    display: flex;
    align-items: center;
    justify-content: center;
`;

export const CardStyled = styled(Card)`
    width: 500px;
    z-index: 1000;
    position: absolute;
`;

export const Header = styled.div`
    font-size: 18px;
    font-weight: bold;
    background-color: black;
    color: white;
    text-align: center;
    padding: 3em;
`;
