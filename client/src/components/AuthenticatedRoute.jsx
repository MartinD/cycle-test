import React, {useContext} from "react";
import { Route, Redirect } from "react-router-dom";
import { AuthContext } from "../context/AuthContext";

const AuthenticatedRoute = props => {
    const { auth } = useContext(AuthContext);
    return auth !== null ? <Route {...props} /> : <Redirect to="/login" />;
};

export default AuthenticatedRoute;
