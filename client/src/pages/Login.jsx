import React, { useState, useContext } from "react";
import { useHistory } from "react-router";
import Button from "@material-ui/core/Button";
import {
    CardContentStyled,
    WrapperTextField,
    Circle,
    TextFieldStyled,
    Container,
    CardStyled,
    Header
} from "../components/Login.style";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { login } from "../actions/auth";
import { AuthContext } from "../context/AuthContext";

const Login = () => {
    const [email, setEmail] = useState("");
    const [password, setPassword] = useState("");
    const [snackOpen, setSnackOpen] = useState(false);
    const [loading, setLoading] = useState(false);
    const [snackStatus, setSnackStatus] = useState("success");
    const history = useHistory();
    const { setAuth } = useContext(AuthContext);

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }

        setSnackOpen(false);
    };

    return (
        <Container>
            <CardStyled>
                <Header>Cycle technical test - Login</Header>
                <CardContentStyled>
                    <WrapperTextField>
                        <TextFieldStyled
                            label="Email"
                            variant="outlined"
                            onChange={e => setEmail(e.target.value)}
                        />
                    </WrapperTextField>
                    <WrapperTextField>
                        <TextFieldStyled
                            label="Password"
                            variant="outlined"
                            type="password"
                            onChange={e => setPassword(e.target.value)}
                        />
                    </WrapperTextField>
                    <Button
                        variant="contained"
                        color="primary"
                        disabled={loading}
                        size="large"
                        onClick={async () => {
                            try {
                                setLoading(true);
                                const res = await login({
                                    email,
                                    password
                                });
                                setAuth(JSON.stringify(res.data.user));
                                history.push("/users");
                                setLoading(false);
                            } catch (err) {
                                setSnackStatus("error");
                                setSnackOpen(true);
                                setLoading(false);
                                throw new Error(err);
                            }
                        }}
                    >
                        {loading ? "Loading..." : "Login"}
                    </Button>
                </CardContentStyled>
            </CardStyled>
            <Circle />
            <Snackbar
                open={snackOpen}
                autoHideDuration={3000}
                onClose={handleClose}
            >
                <Alert onClose={handleClose} severity={snackStatus}>
                    {snackStatus === "success"
                        ? "Welcome!"
                        : "Wrong credentials..."}
                </Alert>
            </Snackbar>
        </Container>
    );
};

export default Login;
