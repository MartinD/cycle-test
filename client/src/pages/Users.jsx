import React, { useContext, useEffect } from "react";
import MaterialTable from "material-table";
import { list, create, update, destroy } from "../actions/user";
import { AuthContext } from "../context/AuthContext";
import { check } from "../utils/rules";

const Users = () => {
    const [users, setUsers] = React.useState([]);
    let { auth } = useContext(AuthContext);
    auth = JSON.parse(auth);
    const columns = [
        {
            title: "Avatar",
            field: "avatar",
            editable: "never",
            render: rowData => {
                let src = "https://cycle.app/public/img/favicon@2x.png";
                if (rowData) {
                    if (rowData.avatar) {
                        src = rowData.avatar;
                    }
                }
                return (
                    <img
                        src={src}
                        style={{ width: 40, borderRadius: "50%", height: 40 }}
                    />
                );
            }
        },
        { title: "Email", field: "email" },
        { title: "First name", field: "firstName" },
        {
            title: "Role",
            field: "role",
            lookup: {
                Admin: "Admin",
                Contributor: "Contributor",
                Reader: "Reader"
            }
        }
    ];

    useEffect(() => {
        list(setUsers);
    }, []);

    const onAdd = check(auth.role, "createUser")
        ? newData =>
              new Promise(resolve => {
                  resolve(
                      create(
                          {
                              email: newData.email,
                              firstName: newData.firstName,
                              role: newData.role
                          },
                          setUsers
                      )
                  );
              })
        : null;

    const onUpdate = check(auth.role, "updateUser")
        ? (newData, oldData) =>
              new Promise(resolve => {
                  resolve(
                      update(
                          oldData.id,
                          {
                              email: newData.email,
                              firstName: newData.firstName,
                              role: newData.role
                          },
                          setUsers
                      )
                  );
              })
        : null;

    const onDelete = check(auth.role, "destroyUser")
        ? oldData =>
              new Promise(resolve => {
                  resolve(destroy(oldData.id, setUsers));
              })
        : null;

    return (
        <MaterialTable
            title="Users"
            columns={columns}
            data={users}
            options={{ pageSize: 10 }}
            editable={{
                isEditable: rowData => rowData.id !== auth.id,
                isDeletable: rowData => rowData.id !== auth.id,
                onRowAdd: onAdd,
                onRowUpdate: onUpdate,
                onRowDelete: onDelete
            }}
        />
    );
};

export default Users;
