import React, { Suspense, lazy, useState, useContext, useEffect } from "react";
import {
    HashRouter as Router,
    Route,
    Switch,
    Redirect
} from "react-router-dom";
import AuthenticatedRoute from "../components/AuthenticatedRoute";
import { AuthContext } from "../context/AuthContext";

const Login = lazy(() => import("./Login"));
const Users = lazy(() => import("./Users"));
const Profile = lazy(() => import("./Profile"));
const Menu = lazy(() => import("../components/Menu"));

const Routes = () => {
    const prevAuth = window.localStorage.getItem("auth") || null;
    const [auth, setAuth] = useState(prevAuth);

    useEffect(() => {
        window.localStorage.setItem("auth", auth);
    }, [auth]);

    return (
        <AuthContext.Provider value={{ auth, setAuth }}>
            <Router>
                <Suspense fallback={<div>Loading...</div>}>
                    <Switch>
                        <Redirect exact from="/" to="/login" />
                        <Route path="/login" component={Login} />
                        <Menu>
                            <Switch>
                                <AuthenticatedRoute
                                    path="/users"
                                    component={Users}
                                />
                                <AuthenticatedRoute
                                    path="/profile"
                                    component={Profile}
                                />
                            </Switch>
                        </Menu>
                    </Switch>
                </Suspense>
            </Router>
        </AuthContext.Provider>
    );
};

export default Routes;
