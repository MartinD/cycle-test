import React, { useContext, useState } from "react";
import { useHistory } from "react-router";
import Button from "@material-ui/core/Button";
import ArrowBackIcon from "@material-ui/icons/ArrowBack";
import Snackbar from "@material-ui/core/Snackbar";
import MuiAlert from "@material-ui/lab/Alert";
import { AuthContext } from "../context/AuthContext";
import {
    CardStyled,
    CardContentStyled,
    WrapperTextField,
    TextFieldStyled,
    Avatar,
    Role,
    ButtonGroup,
    AvatarContainer,
    WrapperButton
} from "../components/Profile.style";
import { update } from "../actions/auth";
import { ChooseAvatar } from "../components/ChooseAvatar";

const Profile = () => {
    const history = useHistory();
    let { auth, setAuth } = useContext(AuthContext);
    auth = JSON.parse(auth);
    const [user, setUser] = useState({
        email: auth.email,
        firstName: auth.firstName,
        avatar: auth.avatar,
        password: "",
        confirmPassword: ""
    });
    const [chooseAvatar, setChooseAvatar] = useState(false);
    const [loading, setLoading] = useState(false);
    const [snackOpen, setSnackOpen] = useState(false);

    const onUpdate = auth => {
        setAuth(auth);
        setLoading(false);
        setSnackOpen(true);
    };

    function Alert(props) {
        return <MuiAlert elevation={6} variant="filled" {...props} />;
    }

    const handleClose = (event, reason) => {
        if (reason === "clickaway") {
            return;
        }
        setSnackOpen(false);
    };

    return (
        <div>
            <Button
                onClick={() => history.push("/users")}
                variant="contained"
                color="primary"
                startIcon={<ArrowBackIcon />}
            >
                Back to users
            </Button>
            <CardStyled>
                <CardContentStyled>
                    {!chooseAvatar ? (
                        <div>
                            <AvatarContainer
                                onClick={() => setChooseAvatar(true)}
                            >
                                <Avatar
                                    src={
                                        user.avatar
                                            ? user.avatar
                                            : "https://cycle.app/public/img/favicon@2x.png"
                                    }
                                />
                            </AvatarContainer>
                            <Role>{auth.role}</Role>
                        </div>
                    ) : (
                        <ChooseAvatar
                            user={user}
                            setUser={setUser}
                            onCancel={() => setChooseAvatar(false)}
                        />
                    )}
                    <WrapperTextField>
                        <TextFieldStyled
                            label="Email"
                            variant="outlined"
                            value={user.email}
                            onChange={e => {
                                let newUser = { ...user };
                                newUser.email = e.target.value;
                                setUser(newUser);
                            }}
                        />
                    </WrapperTextField>
                    <WrapperTextField>
                        <TextFieldStyled
                            label="First name"
                            variant="outlined"
                            value={user.firstName}
                            onChange={e => {
                                let newUser = { ...user };
                                newUser.firstName = e.target.value;
                                setUser(newUser);
                            }}
                        />
                    </WrapperTextField>
                    <WrapperTextField>
                        <TextFieldStyled
                            label="New password"
                            variant="outlined"
                            type="password"
                            value={user.password}
                            onChange={e => {
                                let newUser = { ...user };
                                newUser.password = e.target.value;
                                setUser(newUser);
                            }}
                        />
                    </WrapperTextField>
                    <WrapperTextField>
                        <TextFieldStyled
                            label="Confirm password"
                            variant="outlined"
                            type="password"
                            value={user.confirmPassword}
                            onChange={e => {
                                let newUser = { ...user };
                                newUser.confirmPassword = e.target.value;
                                setUser(newUser);
                            }}
                        />
                    </WrapperTextField>

                    <ButtonGroup>
                        <WrapperButton>
                            <Button
                                variant="outlined"
                                size="large"
                                color="primary"
                                onClick={() =>
                                    setUser({
                                        email: auth.email,
                                        firstName: auth.firstName,
                                        avatar: auth.avatar,
                                        password: "",
                                        confirmPassword: ""
                                    })
                                }
                            >
                                Cancel
                            </Button>
                        </WrapperButton>
                        <WrapperButton>
                            <Button
                                variant="contained"
                                size="large"
                                color="primary"
                                disabled={
                                    user.password !== user.confirmPassword ||
                                    loading
                                }
                                onClick={() => {
                                    setLoading(true);
                                    update({ ...user }, onUpdate);
                                }}
                            >
                                {loading ? "Sending data..." : "Save"}
                            </Button>
                        </WrapperButton>
                    </ButtonGroup>
                </CardContentStyled>
            </CardStyled>
            <Snackbar
                open={snackOpen}
                autoHideDuration={6000}
                onClose={handleClose}
            >
                <Alert onClose={handleClose} severity={"success"}>
                    Profile updated
                </Alert>
            </Snackbar>
        </div>
    );
};

export default Profile;
