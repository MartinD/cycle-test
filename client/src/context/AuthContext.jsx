import React from "react"

export const AuthContext = React.createContext({
    auth: null,
    setAuth: () => {},
});
