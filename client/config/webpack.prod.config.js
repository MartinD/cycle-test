var path = require("path");
const webpack = require("webpack");
var HtmlWebpackPlugin = require("html-webpack-plugin");
var OptimizeCssAssetsPlugin = require("optimize-css-assets-webpack-plugin");
var CopyWebpackPlugin = require("copy-webpack-plugin");
const UglifyJsPlugin = require("uglifyjs-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");

module.exports = {
    entry: {
        main: path.join(__dirname, "..", "src", "index.jsx")
    },
    output: {
        path: path.join(__dirname, "..", "build"),
        chunkFilename: "[name][contenthash].js",
        filename: "[name][contenthash].js"
    },
    resolve: {
        extensions: [".js", ".jsx", ".json", ".css"],
        modules: ["node_modules"]
    },
    module: {
        rules: [
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ["@babel/preset-react"],
                    plugins: ["@babel/plugin-transform-runtime"]
                }
            },
            {
                test: /\.css$/,
                use: [MiniCssExtractPlugin.loader, "css-loader"]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "fonts/"
                        }
                    }
                ]
            },
            { test: /\.(png|jpg)$/i, exclude: /node_modules/, loader: "file" }
        ]
    },
    plugins: [
        new MiniCssExtractPlugin({
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "..", "index.html"),
            inject: "body",
            minify: false
        }),
        new webpack.DefinePlugin({
            "process.env": {
                NODE_ENV: JSON.stringify("production")
            }
        }),
        new webpack.HashedModuleIdsPlugin(),
        new CopyWebpackPlugin(
            [
                {
                    from: path.join(__dirname, "../assets/**/*"),
                    to: "./"
                }
            ],
            {}
        )
    ],
    optimization: {
        minimizer: [
            // we specify a custom UglifyJsPlugin here to get source maps in production
            new UglifyJsPlugin({
                cache: true,
                parallel: true,
                sourceMap: true
            }),
            new OptimizeCssAssetsPlugin({})
        ],
        runtimeChunk: "single",
        splitChunks: {
            chunks: "all",
            maxInitialRequests: Infinity,
            minSize: 0
            // cacheGroups: {
            // 	vendor: {
            // 		test: /[\\/]node_modules[\\/]/,
            // 		name(module) {
            // 			// get the name. E.g. node_modules/packageName/not/this/part.js
            // 			// or node_modules/packageName
            // 			const packageName = module.context.match(
            // 				/[\\/]node_modules[\\/](.*?)([\\/]|$)/
            // 			)[1];

            // 			// npm package names are URL-safe, but some servers don't like @ symbols
            // 			return `npm.${packageName.replace("@", "")}`;
            // 		},
            // 	},
            // },
        }
    }
};
