var path = require("path");
var HtmlWebpackPlugin = require("html-webpack-plugin");
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
const BundleAnalyzerPlugin = require("webpack-bundle-analyzer")
    .BundleAnalyzerPlugin;
const webpack = require("webpack");

module.exports = {
    entry: {
        main: path.join(__dirname, "..", "src", "index.jsx")
    },
    output: {
        path: path.join(__dirname, "..", "build"),
        chunkFilename: "[name][contenthash].js",
        filename: "[name][contenthash].js"
    },
    resolve: {
        extensions: [".js", ".jsx", ".json", ".css"],
        modules: ["node_modules"]
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader
                        // options: {
                        // 	// you can specify a publicPath here
                        // 	// by default it use publicPath in webpackOptions.output
                        // 	publicPath: "../",
                        // },
                    },
                    "css-loader"
                ]
            },
            {
                test: /\.(woff(2)?|ttf|eot|svg)(\?v=\d+\.\d+\.\d+)?$/,
                use: [
                    {
                        loader: "file-loader",
                        options: {
                            name: "[name].[ext]",
                            outputPath: "fonts/"
                        }
                    }
                ]
            },
            {
                test: /\.jsx?$/,
                exclude: /node_modules/,
                loader: "babel-loader",
                query: {
                    presets: ["@babel/preset-react"],
                    plugins: [
                        "@babel/plugin-transform-runtime",
                        "syntax-dynamic-import"
                    ]
                }
            }
        ]
    },
    plugins: [
        new HtmlWebpackPlugin({
            template: path.join(__dirname, "..", "index.html"),
            inject: "body"
        }),
        new MiniCssExtractPlugin({
            // Options similar to the same options in webpackOptions.output
            // both options are optional
            filename: "[name].css",
            chunkFilename: "[id].css"
        }),
        new webpack.HashedModuleIdsPlugin(),
        new BundleAnalyzerPlugin()
    ],
    optimization: {
        runtimeChunk: "single",
        splitChunks: {
            chunks: "all",
            maxInitialRequests: Infinity,
            minSize: 0
            // cacheGroups: {
            // 	vendor: {
            // 		test: /[\\/]node_modules[\\/]/,
            // 		name(module) {
            // 			// get the name. E.g. node_modules/packageName/not/this/part.js
            // 			// or node_modules/packageName
            // 			const packageName = module.context.match(
            // 				/[\\/]node_modules[\\/](.*?)([\\/]|$)/
            // 			)[1];

            // 			// npm package names are URL-safe, but some servers don't like @ symbols
            // 			return `npm.${packageName.replace("@", "")}`;
            // 		},
            // 	},
            // },
        }
    }
};
