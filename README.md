# Cycle technical test

This code base represents the technical test for being part of the Cycle tech team.
The app itself consists in a simple full-stack web app for permissions management.
You can access the app under the following link: http://cycletest-env.eba-n3xmd2mi.eu-west-1.elasticbeanstalk.com/ but will soon be removed.

## Installation

This app runs under Node.js. You will therefore have to install the [Node.js](https://nodejs.org/en/) runtinme with its package manager, npm, first.

Once this is done, you need to run the two following commands to see the app running under your [localhost](http://localhost:8081) on the 8081 port.

```javascript
npm run build
npm run start
```

## Development

If you want to edit the code, first make sure to have the two following tools installed:
* pm2
* sequelize

you can do it with the following command

```javascript
npm i -g pm2 sequelize
```

Then you can start the app with the follwing commands:

```javascript
npm run proxy
npm run dev
```

The app will be running under your [localhost](http://localhost:8081) on the 8000 port.
A bundle analyzer will automatically open to let you see the size of the bundles.

## Contributing

Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.
Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)