const bcryptjs = require("bcryptjs");
const User = require("../models").User;

const SALT_ROUND = 10;

const list = async (req, res, next) => {
    try {
        const users = await User.findAll({
            attributes: ["id", "email", "firstName", "avatar", "role"]
        });
        res.send({ users });
    } catch (err) {
        next(err);
    }
};

const update = async (req, res, next) => {
    try {
        const { userId } = req.params;
        const { email, firstName, role } = req.body;
        const user = await User.findOne({ where: { id: userId }, raw: true });
        if (userId !== req.currentUser.id) {
            const userExists = await existingUser(email);
            if (!userExists || user.email === email) {
                const result = await User.update(
                    { email, firstName, role },
                    { where: { id: userId } }
                );
                if (result) {
                    const users = await User.findAll({
                        attributes: [
                            "id",
                            "email",
                            "firstName",
                            "avatar",
                            "role"
                        ]
                    });
                    res.send({ users: users });
                } else {
                    next({ status: 500, message: "Could not update user" });
                }
            } else {
                next({
                    status: 403,
                    message: "You cannot use the same email twice"
                });
            }
        } else {
            next({ status: 403, message: "You cannot update your own role" });
        }
    } catch (err) {
        next(err);
    }
};

const create = async (req, res, next) => {
    try {
        const { email, firstName, role } = req.body;
        const userExists = await existingUser(email);
        if (!userExists) {
            const salt = bcryptjs.genSaltSync(SALT_ROUND);
            const hashedPassword = bcryptjs.hashSync("password", salt);
            const user = await User.create({
                email: email,
                password: hashedPassword,
                firstName: firstName,
                role: role
            });
            if (user) {
                let users = await User.findAll({
                    attributes: ["id", "email", "firstName", "avatar", "role"]
                });
                res.send({ users });
            } else {
                next({ status: 500, message: "Could not create user" });
            }
        } else {
            next({
                status: 403,
                message: "You cannot use the same email twice"
            });
        }
    } catch (err) {
        next(err);
    }
};

const destroy = async (req, res, next) => {
    try {
        const { userId } = req.params;
        const deleted = await User.destroy({ where: { id: userId } });
        if (deleted) {
            const users = await User.findAll({
                attributes: ["id", "email", "firstName", "avatar", "role"]
            });
            res.send({ users });
        } else {
            next({ status: 500, message: "Could not delete user" });
        }
    } catch (err) {
        next(err);
    }
};

const existingUser = async email => {
    try {
        let users = await User.findAll();
        users = JSON.parse(JSON.stringify(users));
        return users.find(user => user.email === email) !== undefined;
    } catch (err) {
        throw new Error(err);
    }
};

module.exports = {
    list,
    update,
    create,
    destroy
};
