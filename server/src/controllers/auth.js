require("dotenv").config();
const jwt = require("jsonwebtoken");
const bcryptjs = require("bcryptjs");
const User = require("../models").User;

const SALT_ROUND = 10;

const login = async (req, res, next) => {
    const { email, password } = req.body;
    const user = await User.findOne({ where: { email: email }, raw: true });
    if (user) {
        const isSame = bcryptjs.compareSync(password, user.password);
        if (isSame) {
            const token = jwt.sign(
                { id: user.id, email: user.email, role: user.role },
                process.env.TOKEN_SECRET
            );
            res.cookie(
                "access_token",
                { token: token },
                {
                    httpOnly: true
                }
            );
            delete user.password;
            res.send({ message: "User logged in!", user: user });
        } else {
            next({ status: 401, message: "Wrong credentials" });
        }
    } else {
        next({ status: 401, message: "User does not exist" });
    }
};

const logout = (req, res, next) => {
    try {
        req.cookies.set("access_token", { expires: Date.now() });
        res.send({ message: "User logged out" });
    } catch (err) {
        next(err);
    }
};

const update = async (req, res, next) => {
    try {
        const {
            email,
            firstName,
            avatar,
            password,
            confirmPassword
        } = req.body;
        let user = {};
        if (password !== "" && password === confirmPassword) {
            const salt = bcryptjs.genSaltSync(SALT_ROUND);
            const hashedPassword = bcryptjs.hashSync(password, salt);
            user = await User.update(
                { email, firstName, avatar, password: hashedPassword },
                { where: { id: req.currentUser.id }, returning: true }
            );
        } else {
            user = await User.update(
                { email, firstName, avatar },
                { where: { id: req.currentUser.id }, returning: true }
            );
        }
        user = JSON.parse(JSON.stringify(user[1]))[0];
        delete user.password;
        const token = jwt.sign(
            {
                id: req.currentUser.id,
                email: email,
                role: req.currentUser.role
            },
            process.env.TOKEN_SECRET
        );
        res.cookie(
            "access_token",
            { token: token },
            {
                httpOnly: true
            }
        );
        res.send({ message: "User updated", user: user });
    } catch (err) {
        next(err);
    }
};

const verifyToken = (req, res, next) => {
    const token = req.cookies.access_token.token;
    try {
        const decoded = jwt.verify(token, process.env.TOKEN_SECRET);
        req.currentUser = decoded;
        next();
    } catch (err) {
        next(err);
    }
};

module.exports = { login, verifyToken, logout, update };
