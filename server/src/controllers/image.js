require("dotenv").config();
const axios = require("axios");

const searchApi = async (req, res, next) => {
    try {
        const { query } = req.query;
        const result = await axios.get(
            "https://api.unsplash.com/search/photos",
            {
                params: {
                    client_id: process.env.UNSPLASH_ACCESS_KEY,
                    query: query,
                    per_page: 30
                }
            }
        );
        const data = result.data;
        const images = data.results.map(pic => {
            return {
                pictureId: pic.id,
                picture: pic.urls.thumb,
                username: pic.user.username,
                name: pic.user.name
            };
        });
        res.send({ images });
    } catch (err) {
        next(err);
    }
};

const randomApi = async (req, res, next) => {
    try {
        const result = await axios.get("https://api.unsplash.com/photos", {
            params: {
                client_id: process.env.UNSPLASH_ACCESS_KEY,
                per_page: 30
            }
        });
        const data = result.data;
        const images = data.map(pic => {
            return {
                pictureId: pic.id,
                picture: pic.urls.thumb,
                username: pic.user.username,
                name: pic.user.name
            };
        });
        res.send({ images });
    } catch (err) {
        next(err);
    }
};

module.exports = {
    searchApi,
    randomApi
};
