const permissions = require("../utils/rules")
const rules = permissions.rules
const check = permissions.check

const verifyPermissions = (req, res, next) => {
    const baseUrl = req.baseUrl;
    const url = req.url.split("?")[0];
    const ressource = `${baseUrl}${url}`;
    const action = `${req.method}`;

    const user = req.currentUser;

    if (user) {
        const paramRules = rules(req.params);
        const role = user.role;
        if (check(paramRules, role, `${ressource}:${action}`)) {
            next();
        } else {
            next({ status: 403, message: `No such permission` });
        }
    } else {
        next({ status: 401, message: `User should be authenticated` });
    }
};

module.exports = { verifyPermissions }