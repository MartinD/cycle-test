"use strict";

module.exports = {
    up: (queryInterface, Sequelize) => {
        return queryInterface.bulkInsert("Users", [
            {
                id: 1,
                firstName: "Martin",
                avatar: null,
                email: "mrt.dlb@gmail.com",
                password:
                    "$2a$10$nCmtJ0hA3IUT3ZtPoAIdfu2/Qjml3jXnI.ICH7TWshypQaxU9stHK",
                role: "Admin",
                lastLogin: new Date(),
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                id: 2,
                firstName: "Mehdi",
                avatar: null,
                email: "mehdi@cycle.app",
                password:
                    "$2a$10$nCmtJ0hA3IUT3ZtPoAIdfu2/Qjml3jXnI.ICH7TWshypQaxU9stHK",
                role: "Admin",
                lastLogin: new Date(),
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                id: 3,
                firstName: "Benoit",
                avatar: null,
                email: "benoit@cycle.app",
                password:
                    "$2a$10$nCmtJ0hA3IUT3ZtPoAIdfu2/Qjml3jXnI.ICH7TWshypQaxU9stHK",
                role: "Admin",
                lastLogin: new Date(),
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                id: 4,
                firstName: "Olivier",
                avatar: null,
                email: "olivier@cycle.app",
                password:
                    "$2a$10$nCmtJ0hA3IUT3ZtPoAIdfu2/Qjml3jXnI.ICH7TWshypQaxU9stHK",
                role: "Contributor",
                lastLogin: new Date(),
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                id: 5,
                firstName: "Julien",
                avatar: null,
                email: "julien@cycle.app",
                password:
                    "$2a$10$nCmtJ0hA3IUT3ZtPoAIdfu2/Qjml3jXnI.ICH7TWshypQaxU9stHK",
                role: "Reader",
                lastLogin: new Date(),
                updatedAt: new Date(),
                createdAt: new Date()
            },
            {
                id: 6,
                firstName: "Olga",
                avatar: null,
                email: "olga@cycle.app",
                password:
                    "$2a$10$nCmtJ0hA3IUT3ZtPoAIdfu2/Qjml3jXnI.ICH7TWshypQaxU9stHK",
                role: "Reader",
                lastLogin: new Date(),
                updatedAt: new Date(),
                createdAt: new Date()
            }
        ]);
    },

    down: (queryInterface, Sequelize) => {
        return queryInterface.bulkDelete("Users", null, {})
    }
};
