const user = require("../controllers/user");
const auth = require("../controllers/auth");
const right = require("../controllers/right");
const express = require("express");
const router = express.Router();

router.post("/", auth.verifyToken, right.verifyPermissions, user.create);
router.get("/", auth.verifyToken, right.verifyPermissions, user.list);
router.put("/:userId", auth.verifyToken, right.verifyPermissions, user.update);
router.delete(
    "/:userId",
    auth.verifyToken,
    right.verifyPermissions,
    user.destroy
);

module.exports = router;
