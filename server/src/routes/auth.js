const auth = require("../controllers/auth");
const express = require("express");
const router = express.Router();

router.post("/login", auth.login);
router.post("/logout", auth.logout)
router.put("/", auth.verifyToken, auth.update)

module.exports = router;
