const auth = require("./auth");
const user = require("./user");
const image = require("./image");

module.exports = app => {
    app.use("/api/auth", auth);
    app.use("/api/user", user);
    app.use("/api/image", image);
};
