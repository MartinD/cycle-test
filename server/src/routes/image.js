const auth = require("../controllers/auth");
const image = require("../controllers/image");
const express = require("express");
const router = express.Router();

router.get("/search", auth.verifyToken, image.searchApi);
router.get("/random", auth.verifyToken, image.randomApi);

module.exports = router;
