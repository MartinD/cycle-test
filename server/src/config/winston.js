const LoggerStream = require("./LoggerStream");

const winston = new LoggerStream();

const errorHandler = (err, req, res, next) => {
    if (err) {
        winston.logger.error(
            `${err.status || 500} - ${err.message} - ${req.originalUrl} - ${
                req.method
            } - ${req.ip}`
        );
        res.status(err.status || 500).send({ message: "Server error" });
    } else {
        next();
    }
};

module.exports = {
    winston,
    errorHandler
};
