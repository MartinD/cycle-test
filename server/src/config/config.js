require("dotenv").config();

module.exports = {
    development: {
        username: "cycleuser",
        password: process.env.DB_PASSWORD,
        database: "cycle_db",
        host: "cycle-db.c05qm2o18l5m.eu-west-1.rds.amazonaws.com",
        dialect: "postgres",
        operatorsAliases: false
    }
};
