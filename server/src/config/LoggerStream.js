const winston = require("winston");
const appRoot = require("app-root-path");
const createLogger = winston.createLogger;
const transports = winston.transports;

// create a stream object with a 'write' function that will be used by `morgan`
class LoggerStream {
    // define the custom settings for each transport (file, console)
    options = {
        file: {
            level: "info",
            filename: `${appRoot}/server/logs/app.log`,
            handleExceptions: true,
            json: true,
            maxsize: 5242880, // 5MB
            maxFiles: 5,
            colorize: false
        },
        console: {
            level: "debug",
            handleExceptions: true,
            json: false,
            colorize: true
        }
    };

    // instantiate a new Winston Logger with the settings defined above
    logger = createLogger({
        transports: [
            new transports.File(this.options.file),
            new transports.Console(this.options.console)
        ],
        exitOnError: false // do not exit on handled exceptions
    });

    write(message) {
        this.logger.info(message.substring(0, message.lastIndexOf("\n")));
    }
}

module.exports = LoggerStream;
