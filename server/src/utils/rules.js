const rules = params => {
    return {
        Reader: {
            permissions: [
                `/api/user/:GET`,
                `/api/auth/${params.userId}:PUT`
            ]
        },
        Contributor: {
            inherit: "Reader",
            permissions: [`/api/user/${params.userId}:PUT`]
        },
        Admin: {
            inherit: "Contributor",
            permissions: [
                `/api/user/:POST`,
                `/api/user/${params.userId}:DELETE`
            ]
        }
    };
};

const check = (rules, role, action) => {
    const rights = rules[role];
    if (!rights) {
        return false;
    }
    if (rights.permissions && rights.permissions.includes(action)) {
        return true;
    } else if (rights.inherit) {
        return check(rules, rights.inherit, action);
    }
    return false;
};

module.exports = {
    rules,
    check
};
