const bodyParser = require("body-parser");
const express = require("express");
const path = require("path");
const morgan = require("morgan");
const routes = require("./routes");
const winstonConfig = require("./config/winston");
const cookieParser = require("cookie-parser");

const errorHandler = winstonConfig.errorHandler;
const winston = winstonConfig.winston;

const port = process.env.PORT || 8081;

let app = express();
app.use(cookieParser());
app.use(morgan("combined", { stream: winston }));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false })); //to be able to use params from .use routes
routes(app);
app.use(errorHandler);

app.use(express.static(path.resolve(__dirname, "../../client/build"))); // inject client build
app.get("*", res => {
    res.sendFile(path.resolve(__dirname, "../../client/build/index.html"));
    res.end();
});

app.listen(port, () => console.log(`Running on localhost:${port}`));

module.exports = {
    app
};
