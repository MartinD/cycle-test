const chai = require("chai");
const chaiHttp = require("chai-http");
const app = require("../src/index").app;

chai.use(chaiHttp);
chai.should();
var agent = chai.request.agent(app);

describe("Authentication", () => {
    describe("LOGIN", () => {
        it("Should not login", done => {
            agent
                .post("/api/auth/login")
                .set("Content-Type", "application/json")
                .send({ email: "test@mail.com", password: "test" })
                .end((err, res) => {
                    res.should.have.status(401);
                    done();
                });
        });
        it("Should login", done => {
            agent
                .post("/api/auth/login")
                .set("Content-Type", "application/json")
                .send({ email: "test@mail.com", password: "password" })
                .end((err, res) => {
                    res.should.have.cookie("access_token");
                    res.should.have.status(200);
                    done();
                });
        });
        it("Should update auth", done => {
            agent
                .put("/api/auth/")
                .set("Content-Type", "application/json")
                .send({ email: "test@mail.com",  firstName: "tested", avatar: "", password: "", confirmPassword: ""})
                .end((err, res) => {
                    res.should.have.status(200);
                    done();
                });
        });
    });
});
