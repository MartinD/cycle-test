var http = require("http");
var request = require("request");
http
    .createServer(function(req, res) {
        if (req.url.split("/")[1] === "api") {
            console.log("API: ", req.method, req.url);
            req.pipe(request("http://127.0.0.1:8081" + req.url)).pipe(res);
        } else {
            console.log("CLI: ", req.method, req.url);
            req.pipe(request("http://127.0.0.1:8080" + req.url)).pipe(res);
        }
    })
    .listen(8000);